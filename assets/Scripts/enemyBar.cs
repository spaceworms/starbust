﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class enemyBar : MonoBehaviour {

    public Image healthBar;
    public float healthIndex;
    public ship shipScript;

    void Start()
    {
        shipScript = GetComponent<ship>();
    }
    void Update () {
        healthIndex = shipScript.currentHealth / shipScript.maxHealth;
        healthBar.transform.localScale = new Vector2(healthIndex,1);
	}
}
