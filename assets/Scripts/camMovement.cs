﻿using UnityEngine;
using System.Collections;
[System.Serializable]
// !! !!
// WARNING: DO NOT ENABLE BOTH camMovement() and camMovement2()!!!
public class camMovement : MonoBehaviour {

	private GameObject player;
	public float smooth;
	private Vector3 playerpos;
    public float base_zoom;     //minimum size of camera
    public float zoom;        //base_zoom + zoom == maximum size of camera
    private Vector3 last_pos;

	void Awake() {
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void FixedUpdate () {   //fixedupdate makes the camera less laggy
        
		playerpos = new Vector3 (player.transform.position.x, player.transform.position.y , -10);
        last_pos = transform.position;
		transform.position = Vector3.LerpUnclamped (transform.position, playerpos , 1);
        Camera.main.orthographicSize = base_zoom + zoom * Vector3.Magnitude(transform.position - last_pos)/Time.deltaTime;
        //camera size is partly constant and partly varies with velocity;
    }
}
