﻿using UnityEngine;
using System.Collections;

public class enemy : character {

    private float tAngle;
    private float angle;
    public int selectedWeapon;
    private float selfAngle, angle1, angle2, angleToTarget;
    private float tempAngle1, tempAngle2;
    public float finalAngle;
    public GameObject target;
    public Vector3 destination; //temp value
    public Vector3 dir; // dir in moveToDir is the target's velocity vector, in moveToDest is the vector from self to target
    public Vector3 tDir;
    private Vector3 tLastPos, tPos;
    public float k; // used in moveToDir for target leading, 0 <= k <= 1, try 0.8 for optimal result
    void Start()
    {
        spawnShip("enemy");
        target = GameObject.Find("Player Controller");
        tLastPos = target.transform.position;
        tPos = target.transform.position;
        selectedWeapon = 0;
    }
    void Update () {
        if (shipModel)
        {
            transform.position = shipModel.transform.position;
            transform.rotation = shipModel.transform.rotation;

            tPos = target.transform.position;
            dir = tPos - transform.position;

            moveToDir();
            //destination = tPos;
            //moveToDest();

            tLastPos = tPos;
            //shipModel.fire(selectedWeapon);

        }
        else Destroy(this);
        
	}

    // For both movement systems, enemy ships that were in different positions,
    // will eventually end up in the same position, merging into each other
    // This may be solved through introducing random movements -mt

    void moveToDir()    // i came back after a year and i have no idea how this works -mt
    {
        if (tPos!=tLastPos)
            tDir = (tPos - tLastPos) / Time.deltaTime;


        tAngle = Mathf.Atan2(tDir.y, tDir.x) * Mathf.Rad2Deg;
        tAngle = angNormalize(tAngle);
        selfAngle = angNormalize(transform.rotation.eulerAngles.z + 90);
        angleToTarget = angNormalize(Mathf.Atan2((transform.position - tPos).y, (transform.position - tPos).x)*Mathf.Rad2Deg+180);

        angle1 = angNormalize(Mathf.Asin(Mathf.Clamp(Mathf.Sin((tAngle-angleToTarget)*Mathf.Deg2Rad)*k*Vector3.Magnitude(tDir)/shipModel.maxSpeed,-1,1))*Mathf.Rad2Deg);
        angle2 = angNormalize(180 - angle1);
        if (angMin(angle2) > angMin(angle1)) finalAngle = selfAngle - (angleToTarget + angle1);
        else finalAngle = selfAngle - (angleToTarget + angle2);

        finalAngle = -finalAngle;
        if (finalAngle < -180) finalAngle += 360;
        if (finalAngle >= 180) finalAngle -= 360;
        commandMove();
    }
   

    void moveToDest()
    {
        angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        selfAngle = transform.rotation.eulerAngles.z + 90;
        //if (angle < 0)
        //    angle += 360;
        angle = angNormalize(angle);
        finalAngle = angle - selfAngle;

        if (finalAngle > 180)   finalAngle -= 360;
        if (finalAngle < -180)  finalAngle += 360;
        commandMove();

    }

    void commandMove()  //given input of finalAngle, give controls to the ship
    {
        if (Mathf.Abs(finalAngle) > 90) x = -Mathf.Sign(finalAngle);
        else if (Mathf.Abs(finalAngle) < 1) x = 0;
        else x = -Mathf.Atan(finalAngle / 10) * 2 / Mathf.PI;

        if (Vector3.Magnitude(dir) > 5) y = 1;
        else if (Vector3.Magnitude(dir) < 0.1) y = 0;
        else y = Mathf.Atan(Vector3.Magnitude(dir)) * 2 / Mathf.PI;

        shipModel.move(x, y);
    }

    float angMin(float a)   // turns angle a into value between 0 to 180
    {
        if (a >= 180) a = 360 - a;
        if (a < -180) a = 360 + a;
        return a;
    }

    float angNormalize(float a)  //turns angle a into a value between 0 and 360;
    {
        if (a < 0) a += 360;
        if (a >= 360) a -= 360;
        return a;
    }
}
