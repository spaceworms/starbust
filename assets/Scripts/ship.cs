﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ship : MonoBehaviour
{
    public float maxSpeed;
    public float maxHealth, currentHealth;
    public float maxTurnSpeed;
    public weapon[] ownWeapons;
    public string ownerName;
    public float rot; //temp
    public float energy, energyCap, energyGen, inventoryCap;
    public bool notEnoughEnergy = false;

    private ammunition currentShot;
    private Rigidbody2D rb;

    Animator anim;
    bool isDead;
    bool damaged;

    void Awake()
    {
        currentHealth = maxHealth;
    }

    void Start()
    {
        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.drag = 1;
        rb.angularDrag = 0;
        rb.gravityScale = 0;
    }

    public void move(float x, float y)
    {
        // Console will throw a tantrum if the maxSpeed is 0
        float z = transform.rotation.eulerAngles.z; // force rendered when theres physic update, so no use addforce -sw
        z -= x * maxTurnSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, 0, z);

        Vector3 pos = transform.position;
        Vector3 velocity = new Vector3(0, y * maxSpeed * Time.deltaTime, 0);

        pos += transform.rotation * velocity; // a fun thing of quaternion -sw
        transform.position = pos;
    }

    public void fire(int weaponNumber)
    {
        if (energy - ownWeapons[weaponNumber].shot.energyCost >= 0)
        {
            energy -= ownWeapons[weaponNumber].shot.energyCost;
            currentShot = (ammunition)Instantiate(ownWeapons[weaponNumber].shot, rb.transform.position, rb.transform.rotation);
            // can solve by changing array to private, coz unity uses values before configuration (we acan just ignore that) -sw
            currentShot.ownerName = gameObject.GetInstanceID();
        }
    }

    void Update()
    {
        if (energy + energyGen * Time.deltaTime <= energyCap)
        {
            energy += energyGen * Time.deltaTime;
        }
        else
            energy = energyCap;


        if (damaged)
        {
            //damageImage.color = flashColour;
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        damaged = false;
    }


    public void TakeDamage(float amount)
    {

        damaged = true;

        if (currentHealth - amount >= 0)
            currentHealth -= amount;
        else
            currentHealth = 0;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }


    void Death()
    {
        isDead = true;
        Destroy(gameObject);
        //anim.SetTrigger("Die");
    }
}
