﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class player : character {
    private int selectedWeapon;
    public Image healthBar, energyBar;
    public float healthIndex, energyIndex;
    /* 
		currently, the class shipModel is replaced into the instantiated object during Start(),
		this causes ship type to disappear from the script
		if needed an additional variable will be added to prevent such a problem
	*/
    void Start()
    {
        //insert shipSelection()
        spawnShip("ship1");
    }
	void Update () {
		if (shipModel != null) {

			if (Input.GetButtonDown ("Fire1")) {	//fire()
				shipModel.fire (selectedWeapon);
			}
            if (Input.GetMouseButtonDown(1))
            {
                //return mechanism needed!!! -sw
                selectedWeapon++;
                selectedWeapon = selectedWeapon % 2;
            }

			x = Input.GetAxis ("Horizontal");		//movement()
            //turn = Quaternion.AngleAxis(x, shipModel.transform.up);
			y = Input.GetAxis ("Vertical");
			shipModel.move (x, y);
			transform.position = shipModel.transform.position;
			transform.rotation = shipModel.transform.rotation;
		}

        healthIndex = shipModel.currentHealth / shipModel.maxHealth;
        energyIndex = shipModel.energy / shipModel.energyCap;

        healthBar.transform.localScale = new Vector2(healthIndex, 1);
        energyBar.transform.localScale = new Vector2(energyIndex, 1);
    }
}
