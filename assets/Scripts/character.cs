﻿using UnityEngine;
using System.Collections;

public class character : MonoBehaviour {

	public ship shipModel;		//ship to be spawned
    public float x, y;
    public Quaternion turn;
    public Vector3 relativePos;

    //survival down -sw

    public float balance;


    public void spawnShip(string shipname)
    { 
        shipModel = (ship)Instantiate(shipModel, transform.position, transform.rotation);   //spawn ship object
        shipModel.name = shipname;
    }

}
