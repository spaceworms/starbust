﻿using UnityEngine;
using System.Collections;

public class homing : ammunition {
	public float speed;
	public float accuracy; // between 0.5-2 ? -sw
	public float maxTurnSpeed;
    public float range, angle, selfAngle, finalAngle;
    public Transform target;
    private Transform offset;

    void Start()
    {

        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.angularDrag = 0;
        rb.gravityScale = 0;
        rb.velocity = speed * (rb.transform.rotation * Vector2.up); // set speed of projectile
        StartCoroutine(waitFor(0.2f));
        range = maxRange;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.GetInstanceID() != ownerName)
        {
            Destroy(gameObject);
            coll.GetComponent<ship>().TakeDamage(dmg);
        }     
    }

    void Update()
    {
        range -= Time.deltaTime * speed;
        if (range <= 0)
        {
            Destroy(gameObject);
            Destroy(this);
        }
        if (rb.velocity == Vector2.zero)
        {
            if (!target)
            {
                if (GameObject.Find("enemy"))
                {
                    target = GameObject.Find("enemy").GetComponent<Transform>();    //missile repeatedly tries to find a target
                }
                transform.Translate(Vector2.up * Time.deltaTime * speed);           // if no target move forward
            }
            else
            {
                

                Vector3 dir = target.position - transform.position;
                angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + Random.Range(-accuracy, accuracy);
                selfAngle = transform.rotation.eulerAngles.z + 90;
                if (angle < 0)
                    angle += 360;
                finalAngle = angle - selfAngle;
                if (finalAngle > 180)
                    finalAngle -= 360;
                if (finalAngle < -180)
                    finalAngle += 360;

                transform.Rotate(0, 0, Mathf.Atan(finalAngle) * Time.deltaTime * maxTurnSpeed * 45);    // turnspeed here
                transform.Translate(Vector2.up * Time.deltaTime * speed);
            }
        }
    }

    IEnumerator waitFor (float time) //in seconds
    {
        yield return new WaitForSeconds(time);
        rb.velocity = Vector2.zero; // this method is a bit dodgy -mt // ?? -sw //nvm
    }
}

