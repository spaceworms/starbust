﻿using UnityEngine;
using System.Collections;
[System.Serializable]
// !! !!
// WARNING: DO NOT ENABLE BOTH camMovement() and camMovement2()!!!
//camMovement2() camera moves ahead of the player
public class camMovement2 : MonoBehaviour
{

    private GameObject player;
    public float smooth;
    public float base_zoom;     //minimum size of camera
    public float zoom;          //base_zoom + zoom == maximum size of camera
    public float ahead;
    private Vector3 pLastPos,pCurrentPos;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {   //fixedupdate makes the camera less laggy
        pLastPos = pCurrentPos;
        pCurrentPos = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        transform.position = pCurrentPos + (pCurrentPos - pLastPos) / Time.deltaTime * ahead;
        Camera.main.orthographicSize = base_zoom + zoom * Vector3.Magnitude(pCurrentPos - pLastPos) / Time.deltaTime;
        
        
    }
}
