﻿using UnityEngine;
using System.Collections;

public class projectile : ammunition {
	public float speed;
	public float range;


	void Start () {
		rb = gameObject.AddComponent<Rigidbody2D>();
		rb.angularDrag = 0;
		rb.gravityScale = 0;
		rb.velocity =  speed * (rb.transform.rotation * Vector2.up);	// set speed of projectile
		range = maxRange;
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.GetInstanceID() != ownerName)
        {
            Destroy(gameObject);
            coll.GetComponent<ship>().TakeDamage(dmg);
        }
    }


    void Update (){
		range -= Time.deltaTime * speed;
		if (range <= 0) {
			Destroy ( gameObject );
		}
	}
}