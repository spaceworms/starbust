﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneManager : MonoBehaviour {
    private GameObject player;
    private ship shipScript;
    public Image healthBar, energyBar;
    public float healthIndex, energyIndex;

	void Start () {
        player = GameObject.Find("ship1");
        shipScript = player.GetComponent<ship>();
	}
	
	void Update () {

        healthIndex = shipScript.currentHealth / shipScript.maxHealth;
        energyIndex = shipScript.energy / shipScript.energyCap;

        healthBar.transform.localScale = new Vector2(healthIndex,1);
        energyBar.transform.localScale = new Vector2(energyIndex, 1);
	}
}
